import React from 'react'

const Quillbot = () => {
  return(
    <div style={{textAlign: 'center'}}>
      <h1>Hi from Quillbot container route!</h1>
      <div>
        <img src='/og_quillbot.png'/>
      </div>
    </div>
  )
}

export default Quillbot