import type { NextPage } from 'next'
import Head from 'next/head'
import { useEffect } from 'react';

const Home: NextPage = () => {
  useEffect(() => {
    if (typeof window !== 'undefined') {
      const {
        registerMicroApps,
        start,
      } = require('qiankun');

      const microApps = [
        {
          name: 'react',
          entry: '//localhost:3001',
          container: '#Micro',
          activeRule: '/',
        },
      ]

      registerMicroApps(microApps);

      start();
    }
  }, []);

  return (
    <div>
      <Head>
        <title>Quillbot</title>
        <meta name="description" content="Micro-frontend container app" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <div>Server side rendered props</div>
      <div id='Micro'></div>
    </div>
  )
}

export default Home
