import './public-path.js'
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

declare global {
    interface Window {
        __POWERED_BY_QIANKUN__: string
    }
}

function render(props: any) {
    const {container} = props;
    ReactDOM.render(<App/>, container ? container.querySelector('#root') : document.querySelector('#root'));
}

if (!window.__POWERED_BY_QIANKUN__) {
    render({});
}

export async function bootstrap() {
    console.log('[react17] react app bootstraped');
}

export async function mount(props: any) {
    console.log('[react17] props from main framework', props);
    render(props);
}

export async function unmount(props: any) {
    const {container} = props;
    ReactDOM.unmountComponentAtNode(container ? container.querySelector('#root') : document.querySelector('#root'));
}

reportWebVitals();