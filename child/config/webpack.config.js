const packageName = require('./package.json').name;

module.exports = function(webpackEnv) {
  return {
    output: {
      library: `${packageName}-[name]`,
      libraryTarget: 'umd',
      jsonpFunction: `webpackJsonp_${packageName}`,
    }
  }
}